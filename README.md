# **FABIO DOS S. ANJOS**
- [x] http://br.linkedin.com/in/fabioanjosit
- [x] Local: São Paulo - SP


## **SOBRE**
- Experiência de trabalho com metodologia ágil Scrum (Sprint Planning/Review, Daily) para automação de teste.
- Automação de teste sobre um sistema Web de uso interno, no cliente do setor bancário. Sistema Web com rotinas e fluxos de agencia bancária (Previdência, Investimentos, Acordo, Consignado, Cartões, Seguros e outros). Participei da implementação sobre “virtualizar“ (API Rest) os dados de autenticação, para eliminar o uso do login e senha via autenticação por pop-up “Modal”, pois não foi possível interagir no ambiente da Sauce Labs. A automação de teste configurada no Pipeline (Jenkins) é executada no navegador Web, numa sessão da Sauce Labs.
- Automação de testes e tarefas rotineiras em Java JUnit(Selenium/Cucumber/Mockito), Appium, SQL, Mobile (Android) e Web visando sempre a qualidade do software.
- Forte atuação na construção de parcerias internas e na interface entre todas as áreas, otimizando a comunicação, atendimento às necessidades e garantindo a satisfação nos projetos realizados junto aos clientes. 


## **FORMAÇÃO ACADEMICA**

- Pós-graduação Tecnologia da Informação - Universidade Paulista (UNIP) – concluído em 2008.
- Ciência da Computação - Universidade Paulista (UNIP) – concluído em 2001


## **EXPERIÊNCIA PROFISSIONAL**

### **09/2020 - Atual - Yaman - Automatizador de teste**
- Automação de teste Mobile e API RESTful

### **11/2019 – 08/2020 (10 meses) Inmetrics - Jr. Software Development Engineer in Test**
- Automação de testes em Java JUnit/TestNG(Selenium/Cucumber/Mockito) Web, Mobile com Appium (Android). Fazer mock de dados utilizando o DevTest Portal (CA) para aplicação na Web. Levantamento de Cenários para montar o BDD. Utilização do Jenkins para executar jobs agendados de testes automatizados com Maven na Sauce Labs. Excução de testes diretamente na Sauce Labs.

### **03/2019 – 11/2019 (9 meses) MTP Brasil - Analista de infraestrutura de TI**
- Gerenciamento da Infraestrutura de redes, servidores virtuais (VMware ESX) internos Windows server 2008/2012 servidores cloud na Amazon AWS Linux, desktops Linux Ubuntu, Windows 7/8.1/10 Pro, Firewall PFSense com 2 links de internet, programação automatizada dos backups (Shellscript, Powershell e Cobian) de arquivos e VMs (ESX VMWare e O. Virtual Box), OmniaManager (gerenciamento de projetos) e controle de acesso online, banco de dados SQL e MySQL e VPN (OpenVPN) Autenticada com usuários do A.D.. Administração e manutenção de ambientes Cloud(AWS Amazon) e Google Cloud. Monitoramento utilizando Zabbix com Linux CentOS. Instalação/configuração/manutenção servidor de treinamento com Emby server (em Linux) para disponibilizar cursos internos com vídeo aulas. Uso do Trello para gerenciar as tarefas.
- Automação de testes e tarefas rotineiras em Java JUnit(Selenium/Cucumber/Mockito), Appium, SQL, Mobile (Android), Web. Visando sempre a qualidade do software.

### **11/2018 – 03/2019 (5 meses) Sistemas TH - Analista de infraestrutura de TI**
- Administração/Manutenção/Configuração/Backup (Cobian) Sistemas web com IIS do Windows Server e MS SQL Server. Monitoramento 24x7 de sistemas web, com ferramentas desenvolvidas internamente. Uso do Trello para gerenciar as tarefas e projetos.

### **10/2016 – 11/2018 (2anos e 2 meses) MTP Brasil - Analista de Infraestrutura**
- Responsável por Infraestrutura de redes, servidores virtuais (VMware ESX) internos Windows server 2008/2012 servidores cloud na Amazon AWS Linux, desktops Linux Ubuntu, Windows 7/8.1/10 Pro, Firewall PFSense com 2 links de internet, programação automatizada dos backups (Shellscript, Powershell e Cobian) de arquivos e VMs (ESX VMWare e O. Virtual Box), OmniaManager (gerenciamento de projetos) e controle de acesso online, banco de dados SQL e MySQL e VPN (OpenVPN) Autenticada com usuários do A.D.. Administração e manutenção de ambientes Cloud(AWS Amazon) e Google Cloud. Automação de testes, avaliando a qualidade softwares na plataforma Android Mobile com uso programação em Java JUnit (Selenium/Cucumber/Mockito), Appium, em Emuladores de Android em dispositivos variados.

### **04/2005 - 06/2015 (10 anos) Luandre Serviços Temp. - Analista de suporte III**

***02/2012 - 06/2015 (3 anos e 5 meses) Analista de suporte III.***
- Responsável de uma equipe com 4 analistas de suporte técnico para atender 340 clientes (internos) entre matriz e filiais, administração de redes. E também fazer o atendimento ao cliente externo, com implantação e manutenção de sistemas em rede, gestão de equipes e implementação de melhorias. Seguindo as práticas do ITIL, envolvendo o controle de demandas, definição do cronograma (MS Project), análise da viabilidade, negociação de prazos e escopo, assegurando o cumprimento dos prazos, custos e qualidade requerida pela organização.

***08/2008 - 02/2012 (3 anos e 7 meses) Analista de suporte II***
- Suporte técnico para atender aos clientes internos (340 colaboradores) e também clientes externos (10 clientes) com a infraestrutura de implantação de relógios de ponto Dimep e Trix.
- Responsável por fazer instalação, configuração, manutenção e suporte no software de gestão G.Infor que opera com o banco de dados Microsoft SQL Server 2008. E Administração da rede na Matriz e Filiais com servidores Windows e Linux.


***04/2005 - 07/2008 (3 anos e 4 meses) Analista de Suporte I***
- Administração de roteadores Cisco 2500/1700, servidores Windows 2000/2003/2008, Linux Debian, Ubuntu, RedHat/Gentoo, SQL Server2000/2005/2008, Terminal Server Windows 2000 Server, Jabber, RequestTracker, Ocomon, Apache, DNS (named), Samba, Nis, Squid, Iptables, Firewall Endian, VPN,MySql, ShellScript, Exim, Nagios, Cacti, Intranet, montagem de micros, configurações e manutenção de Hardware, Rotinas de Backup em HDs, Fitas LTO e DVD. Virtualização de servidores e desktops com VirtualBox. Desenvolvimento de sistema web comASP.NET Framework4.0 e SQL.


## **CURSOS**
- Testes funcionais com Selenium WebDriver: Do básico ao GRID   (UDEMY)             
- React Native e Expo (backend & frontend) (em curso)   (RocketSeat)
- Linux Center Tec. e Linux Center Network  (UTAH)		
- Inglês/Espanhol Básico. (em curso )   (Duolingo)	
- 2778 – Wr. Quer. Using MS SQL Server 2008 Trans. SQL  (GREEN)
- 6231/10142 – Manut.  B. D. do Microsoft SQL Server 2008   (GREEN)	             
- 6232/10169 – Impl. B. D. do Microsoft SQL Server 2008 (GREEN)


## **TECNOLOGIAS**
- [x] S. O.: Windows server 2003/2008/2012, Windows 7/8.1/10, FreeBSD, UNIX, Linux (Debian, Ubuntu e CentOS, Fedora)
- [x] Serviços/Servidor: Proxy Squid, Servidores Web (Apache e IIS), ESX VMWare (Virtualização), VirtualBox (Virtualização) e XenServer (Virtualização) Google Cloud (Cloud Computing), AWS Amazon, VPN, Git, Github e Gitlab, Jenkins, SV DevTest Portal Broadcom.
- [x] Banco de dados: SQL Server, Mysql
- [x] Programação: Java, JUnit, Selenium, Appium, Cucumber, HTML, ASP, VB.Net, ASP.Net, C#
- [x] Ferramentas: LibreOffice, Open Office, Microsoft Office 2013/2016/365, Ms. Project, Ms. Visio, Trello, Jira.


## **Projetos: Automação de teste (desafios)**

###  https://gitlab.com/users/fabioanjosit/projects
- [x] https://gitlab.com/fabioanjosit/test-web-shoestock
- [x] https://gitlab.com/fabioanjosit/test-api-trello
###  https://github.com/heoxzy
- [x] https://github.com/heoxzy/TestGTW 
- [x] https://github.com/heoxzy/INM-REQRES-API 

